import { Body, Controller, Get, Param, Post, Request, UseGuards, UsePipes } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto, UserCredentialsDto } from '../dtos/users.dto';
import { User } from '../schemas/users.schema';
import { SimpleValidationPipe } from '../pipes/simple-validation.pipe';
import { CreateUserSchema } from '../schemas/create-user.joi.schema';
import { AuthenticateUserSchema } from '../schemas/authenticate-user.joi.schema'
import { HashPasswordPipe } from 'src/pipes/hash-password.pipe';
import { LocalAuthGuard } from './local-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Post()
  @UsePipes(new SimpleValidationPipe(AuthenticateUserSchema))
  async authenticate(@Body() userCredentialsDto: UserCredentialsDto) : Promise<any> {
    await this.authService.authenticate(userCredentialsDto);
  }

  @Post('register')
  @UsePipes(new SimpleValidationPipe(CreateUserSchema), new HashPasswordPipe())
  async create(@Body() createUserDto: CreateUserDto): Promise<any> {
    await this.authService.create(createUserDto);
  }

//   @Get()
//   async get(): Promise<Array<Product>> {
//       return this.productsService.findAll();
//   }

//   @Get(':id')
//   async getID(@Param('id') id: string): Promise<Product> {
//     return this.productsService.findByID(id);
//   }
}