import { Injectable, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { CreateUserDto, UserCredentialsDto } from '../dtos/users.dto';
import * as bcrypt from 'bcrypt';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/schemas/users.schema';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService,
              private jwtService: JwtService) {}

  async create(createUserDto: CreateUserDto): Promise<any> {
    return this.usersService.create(createUserDto);
  }

  async authenticate(credentials: UserCredentialsDto): Promise<User> {
    const user = await this.usersService.findOne(credentials.email);
    const result = await bcrypt.compare(credentials.password, user.password); 
    if(!result)
        throw new UnauthorizedException();
    return user;
  }

  async authorize(token): Promise<Boolean> {
      return true;
  }

  async login(user: any) {
    const payload = { email: user.email, id: user._id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

}