import { Body, Controller, Get, Param, Post, UseGuards, UsePipes } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from '../dtos/create-product.dto';
import { Product } from '../schemas/products.schema';
import { SimpleValidationPipe } from '../pipes/simple-validation.pipe';
import { CreateProductSchema } from '../schemas/create-product.joi.schema';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post()
  @UsePipes(new SimpleValidationPipe(CreateProductSchema))
  async create(@Body() createProductDto: CreateProductDto): Promise<any> {
    await this.productsService.create(createProductDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async get(): Promise<Array<Product>> {
      return this.productsService.findAll();
  }

  @Get(':id')
  async getID(@Param('id') id: string): Promise<Product> {
    return this.productsService.findByID(id);
  }
}