import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateCarDto } from 'src/dtos/create-car.dto';
import { Repository } from 'typeorm';
import { Car } from './cars.entity';

@Injectable()
export class CarsService {
  constructor(
    @InjectRepository(Car)
    private carsRepository: Repository<Car>,
  ) {}

  findAll(): Promise<Car[]> {
    return this.carsRepository.find();
  }

  findOne(id: string): Promise<Car> {
    return this.carsRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.carsRepository.delete(id);
  }

  async createOne(car: CreateCarDto): Promise<any> {
      const result = await this.carsRepository.insert(car);
      console.log(result);
  }
}