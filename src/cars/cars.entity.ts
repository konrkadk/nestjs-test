import { Entity, PrimaryColumn, ManyToOne, JoinTable, ManyToMany } from 'typeorm';
import { Engine } from '../engines/engines.entity';
import { Manufacturer } from '../manufacturers/manufacturers.entity';

@Entity()
export class Car {
  @PrimaryColumn()
  model: string;

  @ManyToOne(() => Manufacturer, manufacturer => manufacturer.cars)
  manufacturer: Manufacturer;

  @ManyToMany(() => Engine)
  @JoinTable()
  engines: Engine[];
}