import { Body, Controller, Get, Post, UseGuards, UsePipes } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { CreateCarDto } from '../dtos/create-car.dto';
import { SimpleValidationPipe } from '../pipes/simple-validation.pipe';
import { CreateCarSchema } from '../schemas/create-car.joi.schema';
import { CarsService } from './cars.service';

@Controller('cars')
export class CarsController {
    constructor(private carsService: CarsService) {}

    @UseGuards(JwtAuthGuard)
    @Post()
    @UsePipes(new SimpleValidationPipe(CreateCarSchema))
    async create(@Body() createCarDto: CreateCarDto): Promise<any> {
        this.carsService.createOne(createCarDto);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getCars(): Promise<any> {
        return this.carsService.findAll();
    }
}
