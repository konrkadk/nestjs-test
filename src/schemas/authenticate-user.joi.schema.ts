import * as Joi from '@hapi/joi'

export const AuthenticateUserSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required()
});