import * as Joi from '@hapi/joi'

export const CreateUserSchema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  dob: Joi.string().required(),
  gender: Joi.string().optional()
});