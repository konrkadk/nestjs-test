import * as Joi from '@hapi/joi'

export const CreateManufacturerSchema = Joi.object({
  code: Joi.string().required(),
  name: Joi.string().required()
});
