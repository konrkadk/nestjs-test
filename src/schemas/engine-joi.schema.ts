import * as Joi from '@hapi/joi'

export const CreateEngineSchema = Joi.object({
  code: Joi.string().required(),
  displacement: Joi.number().required(),
  power: Joi.number().required(),
  manufacturer: Joi.string().required()
});
