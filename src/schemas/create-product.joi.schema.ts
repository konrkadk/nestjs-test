import * as Joi from '@hapi/joi'

export const CreateProductSchema = Joi.object({
  name: Joi.string().required(),
  price: Joi.number().required(),
  manufacturer: Joi.string().optional(),
  quantity: Joi.number().required()
});