import * as Joi from '@hapi/joi'

export const CreateCarSchema = Joi.object({
  make: Joi.string().required(),
  model: Joi.string().required(),
  colour: Joi.string().required(),
  price: Joi.number().required(),
});