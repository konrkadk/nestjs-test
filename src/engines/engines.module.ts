import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnginesController } from './engines.controller';
import { EnginesService } from './engines.service';
import { Engine } from './engines.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Engine])],
  providers: [EnginesService],
  controllers: [EnginesController],
})
export class EnginesModule {}