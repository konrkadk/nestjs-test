import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateEngineDto } from 'src/dtos/engines.dto';
import { Repository } from 'typeorm';
import { Engine } from './engines.entity';

@Injectable()
export class EnginesService {
  constructor(
    @InjectRepository(Engine)
    private enginesRepository: Repository<Engine>,
  ) {}

  findAll(): Promise<Engine[]> {
    return this.enginesRepository.find();
  }

  findOne(id: string): Promise<Engine> {
    return this.enginesRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.enginesRepository.delete(id);
  }

  async createOne(engine: CreateEngineDto): Promise<any> {
      const result = await this.enginesRepository.insert(engine);
      console.log(result);
  }
}