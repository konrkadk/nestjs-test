import { Entity, Column, PrimaryColumn, ManyToOne } from 'typeorm';
import { Manufacturer } from '../manufacturers/manufacturers.entity';
import { Car } from '../cars/cars.entity';

@Entity()
export class Engine {
    @PrimaryColumn()
    code: string;

    @Column()
    displacement: number;

    @Column()
    power: number;

    // one to many relation with the manufacturer
    @ManyToOne(() => Manufacturer, manufacturer => manufacturer.engines)
    manufacturer: Manufacturer;

    //many to many relation with the Car(Model)
}