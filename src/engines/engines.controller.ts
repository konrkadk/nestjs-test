import { Body, Controller, Get, Post, UseGuards, UsePipes } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateEngineDto } from 'src/dtos/engines.dto';
import { SimpleValidationPipe } from 'src/pipes/simple-validation.pipe';
import { CreateEngineSchema } from '../schemas/engine-joi.schema';
import { EnginesService } from './engines.service';

@Controller('engines')
export class EnginesController {
    constructor(private engineService: EnginesService) {}

    @UseGuards(JwtAuthGuard)
    @Post()
    @UsePipes(new SimpleValidationPipe(CreateEngineSchema))
    async create(@Body() createEngineDto: CreateEngineDto): Promise<any> {
        this.engineService.createOne(createEngineDto);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async getAll(): Promise<any> {
        return this.engineService.findAll();
    }
}
