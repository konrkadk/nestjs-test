import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from 'src/dtos/users.dto';

@Injectable()
export class HashPasswordPipe implements PipeTransform<CreateUserDto, Promise<CreateUserDto>> {
  async transform(value: CreateUserDto, metadata: ArgumentMetadata): Promise<CreateUserDto> {
    value.password = await bcrypt.hash(value.password, 10);
    return value;
  }
}