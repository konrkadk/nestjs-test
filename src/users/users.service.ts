import { Injectable, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from '../schemas/users.schema';
import { CreateUserDto, UserCredentialsDto } from '../dtos/users.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const createdUser = new this.userModel(createUserDto);
    return createdUser.save();
  }

  async findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  async authenticate(credentials: UserCredentialsDto): Promise<Boolean> {
    let user;
    try{
        user = await this.userModel.findOne({email: credentials.email});
    } catch(err) {
        throw new UnauthorizedException();
    }
    const result = await bcrypt.compare(credentials.password, user.password); 
    if(!result)
        throw new UnauthorizedException();
    return true;
  }

  async authorize(token): Promise<Boolean> {
      return true;
  }

  async findByID(id): Promise<User> {
    try {
        const result = await this.userModel.findById(id);
        return result;
    } catch(err) {
        throw new BadRequestException();
    }
  }

  async findOne(email: string) : Promise<User> {
      try {
          const user = await this.userModel.findOne({email});
          return user;
      } catch(err) {
          throw new UnauthorizedException();
      }
  }
}