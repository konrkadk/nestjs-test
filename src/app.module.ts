import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
import { CarsModule } from './cars/cars.module';
import { ManufacturersModule } from './manufacturers/manufacturers.module';
import { EnginesModule } from './engines/engines.module';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/nest'),
            TypeOrmModule.forRoot({
              "type": "postgres",
              "host": "localhost",
              "port": 5555,
              "username": "postgres",
              "password": "postgres_password",
              "database": "nesttest",
              "logging": false,
              autoLoadEntities: true,
              synchronize: false,
            }),
            ProductsModule,
            AuthModule,
            UsersModule,
            CarsModule,
            ManufacturersModule,
            EnginesModule],
})
export class AppModule {}
