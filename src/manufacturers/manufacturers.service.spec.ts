import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Manufacturer, ManufacturerRepositoryFake } from './manufacturers.entity';
import {
  CreateManufacturerDto
} from '../dtos/manufacturers.dto';
import { ManufacturersService } from './manufacturers.service';

describe('Manufacturer Service', () => {
  let manufacturerService: ManufacturersService;
  let manufacturerRepository: Repository<Manufacturer>;

  beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          ManufacturersService,
          {
            provide: getRepositoryToken(Manufacturer),
            useClass: ManufacturerRepositoryFake,
          },
        ],
      }).compile();

      manufacturerService = module.get(ManufacturersService);
      manufacturerRepository = module.get(getRepositoryToken(Manufacturer));
  });

  describe('creating a manufacturer', () => {
      it('successfully creates a manufacturer', async () => {
        const code = 'WBA';
        const name = 'BMW';

        const manufacturerData: CreateManufacturerDto = {
            code,
            name
        };
        //these two should ideally differ but since there is no auto generated id
        //or any other data in that entity this will have to do
        const createdManufacturerEntity = Manufacturer.of(manufacturerData);
        const savedManufacturer = Manufacturer.of(manufacturerData);
        
        const manufacturerRepositorySaveSpy = jest
            .spyOn(manufacturerRepository, 'save')
            .mockResolvedValue(savedManufacturer);
        
        const manufacturerRepositoryCreateSpy = jest
            .spyOn(manufacturerRepository, 'create')
            .mockReturnValue(createdManufacturerEntity);

        const result = await manufacturerService.createOne(manufacturerData);

        expect(manufacturerRepositoryCreateSpy).toBeCalledWith(manufacturerData);
        expect(manufacturerRepositorySaveSpy).toBeCalledWith(createdManufacturerEntity);
        expect(result).toEqual(savedManufacturer);
      });
  });
});