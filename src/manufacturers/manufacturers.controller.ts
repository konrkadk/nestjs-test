import { Body, Controller, Get, Post, UseGuards, UsePipes } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateManufacturerDto } from '../dtos/manufacturers.dto';
import { SimpleValidationPipe } from 'src/pipes/simple-validation.pipe';
import { CreateManufacturerSchema } from 'src/schemas/manufacturer-joi.schema';
import { ManufacturersService } from './manufacturers.service';

@Controller('manufacturers')
export class ManufacturersController {
    constructor(private manufacturersService: ManufacturersService) {}

    @UseGuards(JwtAuthGuard)
    @Post()
    @UsePipes(new SimpleValidationPipe(CreateManufacturerSchema))
    async create(@Body() createManufacturerDto: CreateManufacturerDto): Promise<any> {
        this.manufacturersService.createOne(createManufacturerDto);
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async get(): Promise<any> {
        return this.manufacturersService.findAll();
    }
}
