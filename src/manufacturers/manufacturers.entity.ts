import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Engine } from '../engines/engines.entity';
import { Car } from '../cars/cars.entity';

@Entity()
export class Manufacturer {
  @PrimaryColumn()
  code: string;

  @Column()
  name: string;

  //all the relation between Manufacturer and other tables are kept on the other side
  @OneToMany(() => Engine, engine => engine.manufacturer)
  engines: Engine[];

  @OneToMany(() => Car, car => car.manufacturer)
  cars: Car[];

  public static of(params: Partial<Manufacturer>): Manufacturer {
    const manufacturer = new Manufacturer();
    Object.assign(manufacturer, params);
    return manufacturer;
  }
}

export class ManufacturerRepositoryFake {
  public create(): void {}
  public async save(): Promise<void> {}
  public async remove(): Promise<void> {}
  public async findOne(): Promise<void> {}
}