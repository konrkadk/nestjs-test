import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { create } from 'domain';
import { CreateManufacturerDto, UpdateManufacturerDto } from 'src/dtos/manufacturers.dto';
import { Repository } from 'typeorm';
import { Manufacturer } from './manufacturers.entity';

@Injectable()
export class ManufacturersService {
  constructor(
    @InjectRepository(Manufacturer)
    private manufacturersRepository: Repository<Manufacturer>,
  ) {}

  findAll(): Promise<Manufacturer[]> {
    return this.manufacturersRepository.find();
  }

  findOne(id: string): Promise<Manufacturer> {
    return this.manufacturersRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.manufacturersRepository.delete(id);
  }

  async createOne(manufacturerDto: CreateManufacturerDto): Promise<Manufacturer> {
    const manufacturer = this.manufacturersRepository.create({
        ...manufacturerDto
    });
    const createdManufacturer = await this.manufacturersRepository.save(manufacturer);
    return createdManufacturer;
  }

}