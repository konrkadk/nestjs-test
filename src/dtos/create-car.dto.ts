export class CreateCarDto {
    make: string;  
    model: string;  
    colour: string;
    price: number;  
}