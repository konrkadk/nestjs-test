export class CreateManufacturerDto {
    code: string;  
    name: string; 
}

export class UpdateManufacturerDto {
    name: string;
}