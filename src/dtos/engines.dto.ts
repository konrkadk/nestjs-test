import { Manufacturer } from "src/manufacturers/manufacturers.entity";

export class CreateEngineDto {
    code: string;  
    displacement: number;
    power: number;
    manufacturer: Manufacturer;
}