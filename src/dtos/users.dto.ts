
export class CreateUserDto {
    name: string;  
    dob: string;  
    gender: string;
    email: string;
    password: string;
}

export class UserCredentialsDto {
    email: string;
    password: string;
}